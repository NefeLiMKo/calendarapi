from typing import Any
from django.contrib.auth import authenticate, login, get_user_model
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, status
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

from .models import User
from .serializers import UserRegistrationSerializer, UsersListSerializer, UserLoginSerializer

class UsersListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UsersListSerializer

    def get(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class UserRegistrationView(generics.GenericAPIView):
    serializer_class = UserRegistrationSerializer
    queryset = User.objects.all()

    def post(self, request: Request, *args: Any, **kwargs: Any) -> Response:

        data = request.data
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST, content_type=serializer.errors)
        serializer.create(serializer.data)
        return Response(data=data, status=status.HTTP_201_CREATED)


class UserLoginView(APIView):
    permission_classes = [AllowAny]

    def post(self, request: Request, *args, **kwargs) -> Response:
        data = request.data
        serializer = UserLoginSerializer(data=data)
        print("validation")
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            print(new_data)
            if new_data["email"] != '':
                user = authenticate(request, email=new_data["email"], password=new_data["password"])
            if new_data["username"] != '':
                user = authenticate(request, username=new_data["username"], password=new_data["password"])
            if user is not None:
                print("not none")
                login(request, user)
            token, _ = Token.objects.get_or_create(user=user)
            return Response({'token': token.key}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
