from django.urls import path, include

from .views import UserRegistrationView, UsersListView, UserLoginView

urlpatterns = [
    path('registration/', UserRegistrationView.as_view(), name='registration'),
    path('users_list/', UsersListView.as_view(), name='users_list'),
    path('login/', UserLoginView.as_view(), name='login')
]
