from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model,login,authenticate
from django.db.models import Q

from rest_framework import serializers


from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'username', )


class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username',  'email', 'email2', 'password')


class UsersListSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
    


class UserLoginSerializer(serializers.ModelSerializer):
    token = serializers.CharField(allow_blank=True, read_only=True)
    username = serializers.CharField(required=False, allow_blank=True)
    email = serializers.EmailField(required=False,allow_blank=True, label='Email Address')


    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'token')

    def validate(self, data):
        email = data.get("email", None)
        username = data.get("username", None)
        password = data.get("password")
        if not email and not username:
            raise serializers.ValidationError("Username or email is required")
        user = User.objects.filter(Q(email=email) | Q(username=username)).distinct()
        user = user.exclude(email__isnull=True).exclude(email__iexact='')
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise serializers.ValidationError("Username/email is not valid")
        if user_obj:
            if not user_obj.check_password(password):
                raise serializers.ValidationError("Incorrect")
        if not email:
            data["email"] = ''
        if not username:
            data["username"] = ''
        data["token"] = "rand"
        return data



